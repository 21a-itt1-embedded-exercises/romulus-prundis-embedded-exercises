#We use the print for uploading in the Shell "Hello world", which is outside the loop

print("hello world!\n")
#The next one is a loop, that will repeat 10 times, the i being the variable assign in the loop
for i in range(15, 33):    #the column or (:) tell Thonny the loop begins on next line.
    #Using the print function we will print the message underneath, and calling the variable i
    print("What is your name? My name is Shortmemory, me memory lasts\n", i)  
#As it is in cascade, the intingure will show after the loop is over (until 12), it seats outside the loop
print("I think we meet before\n")
