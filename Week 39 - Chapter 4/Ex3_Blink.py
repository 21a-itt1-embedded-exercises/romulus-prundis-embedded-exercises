import machine
import utime
# The number 15 is from the Pico (the pin 20) or GP15, but we don't use the GP in front in programming. GP (are output-input). While GND is for negative.
led_external = machine.Pin(15, machine.Pin.OUT)
led_onboard = machine.Pin(25, machine.Pin.OUT)

while True:
    led_onboard.toggle()
    utime.sleep(0.02)
    
    led_external.toggle()
    utime.sleep(10)