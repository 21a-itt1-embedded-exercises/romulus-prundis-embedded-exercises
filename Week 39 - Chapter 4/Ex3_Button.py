import machine
import utime

button = machine.Pin(14, machine.Pin.IN)

while True:
    if button.value() == 1:
        print("Did you pressed the button?")
        utime.sleep(2)
