# micropython quick ref for RP2 https://docs.micropython.org/en/latest/rp2/quickref.html#adc-analog-to-digital-conversion
 # micropython ADC class https://docs.micropython.org/en/latest/library/machine.ADC.html#machine-adc

from machine import ADC, Pin
from time import sleep

adc0 = ADC(Pin(26))     # create ADC object on ADC GPIO 26
adc1 = ADC(Pin(27))     # create ADC object on ADC GPIO 27

while True:
     potentiometer = adc0.read_u16()         # read value, 0-65535 across voltage range 0.0v - 3.3v
     ldr = adc1.read_u16()                   # read value, 0-65535 across voltage range 0.0v - 3.3v    
     print(potentiometer, ldr)
     sleep(0.1)