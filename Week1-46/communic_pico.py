from smbus import SMBus
from time import sleep

# comms
addrArduino = 0x9 # the I2C address we chose for Arduino - defined in .ino over inside Arduino
bus = SMBus(1) # the I2C bus to use (RPi has more than 1)

# command addresses
fw = 0x0
bw = 0x1
stop = 0x2
